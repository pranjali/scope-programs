import sys
import unittest

# Author: Pranjali Pokharel


class TestXMethod(unittest.TestCase):
    """
    Test the drawing of X
    Tests 0, 1, 5
    :return:
    """

    def setUp(self):
        self.zero_height = ""
        self.one_height = "*"
        self.five_height = "*   *\n" + " * *\n" + "  *\n" + " * *\n" + "*   *"

    def test_height_zero(self):
        x_zero = draw_x(0)
        self.assertEquals(x_zero, self.zero_height)

    def test_height_one(self):
        x_one = draw_x(1)
        self.assertEquals(x_one, self.one_height)

    def test_height_five(self):
        x_five = draw_x(5)
        self.assertEquals(x_five, self.five_height)


def draw_x(height):
    """
    Function that draws an X using the height provided.
    :param height:
    :return:
    """
    if height == 0:
        return ""
    halfway_point = int(height / 2)
    x_drawing = ""
    for i in range(1, halfway_point + 1):
        number_of_leading_spaces = i - 1
        leading_spaces = " " * number_of_leading_spaces
        number_of_middle_spaces = height - (i * 2)
        middle_spaces = " " * number_of_middle_spaces
        x_drawing += leading_spaces + "*" + middle_spaces + "*\n"
    x_drawing += (" " * halfway_point) + "*\n"
    for j in range(1, halfway_point + 1):
        number_of_leading_spaces = halfway_point - j
        leading_spaces = " " * number_of_leading_spaces
        number_of_middle_spaces = 2 * j - 1
        middle_spaces = " " * number_of_middle_spaces
        x_drawing += leading_spaces + "*" + middle_spaces + "*\n"
    return x_drawing[:-1]


if __name__ == "__main__":
    """
    Main function that runs the program. 
    Keep asking for input until the user provides an integer
    If the user ran the test command, then run the tests instead of the main program
    """
    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        print("-----Running X tests-----")
        test_suite = unittest.TestLoader().loadTestsFromTestCase(TestXMethod)
        unittest.TextTestRunner(verbosity=2).run(test_suite)
    else:
        while True:
            try:
                x_height = int(input("Please enter the height of your X: "))
                x = draw_x(x_height)
                print(x)
                break
            except ValueError:
                print("Invalid input. Please, enter the X height in odd integer format")
