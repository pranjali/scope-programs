# Scope Programs

This repository contains two Python programs, one that draws a Christmas tree and the second draws an X both of arbitrary height that is inputted by the user. It also contains unit tests for these two programs. 

Requirements: Make sure you have Python 3.x.x installed on your computer, and that it can be run via the "python" command in the terminal or command line. 

# Running the program #

*Christmas Tree:*
To run the Christmas tree program, download the "tree.py" file, go to the location of the file via the terminal, and then type in "python tree.py" in the terminal.


*X of arbitrary height:*
To run the X drawing program, download the "x.py" file, go to the location of the file via the terminal, and then type in "python x.py" to run the program. 

Both these programs will ask the user for the height to input. This height must be entered in integer format for the program to accept it. 

# Testing the program #

*Christmas Tree:*
To run the unit tests for the Christmas tree program, run the command "python tree.py test" via the terminal from the file directory. 

*X of arbitrary height*
To run the unit tests for the X drawing program, run the command "python x.py test" via the terminal from the file directory.