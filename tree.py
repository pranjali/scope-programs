import sys
import unittest

# Author: Pranjali Pokharel


class TestTreeMethod(unittest.TestCase):
    """
    Test the drawing of Christmas tree.
    Tests 0, 1, an even number, and an odd number
    :return:
    """

    def setUp(self):
        self.zero_height = ""
        self.one_height = "*"
        self.two_height = " *\n" + "***"
        self.five_height = "    *\n" + "   ***\n" + "  *****\n" + " *******\n" + "*********"

    def test_height_zero(self):
        tree_drawn_zero = draw_tree(0)
        self.assertEquals(tree_drawn_zero, self.zero_height)

    def test_height_one(self):
        tree_drawn_one = draw_tree(1)
        self.assertEquals(tree_drawn_one, self.one_height)

    def test_height_even(self):
        tree_drawn_two = draw_tree(2)
        self.assertEquals(tree_drawn_two, self.two_height)

    def test_height_odd(self):
        tree_drawn_five = draw_tree(5)
        self.assertEquals(tree_drawn_five, self.five_height)


def draw_tree(height):
    """
    Function that draws the Christmas tree using the height provided.
    :param height:
    :return tree_string:
    """
    tree_string = ""
    # Start range from 1 because at height=0 there is no tree
    # End range at height+1 to print all the lines including the max width line
    for i in range(1, height + 1):
        # The line at the top will have one space more in the front than the one after and so on
        number_of_spaces = height - i
        space = " " * number_of_spaces
        # Total number of stars per line follows this pattern: 1, 3, 5, 7,...,n
        # Line 1 will always have 1 star (1=1*2-1), line 2 will have 3 stars (3 = 2*2-1),
        # Line 3 will always have 5 stars (5=3*2-1), we use this pattern to come up with a formula
        number_of_stars = i * 2 - 1
        stars = "*" * number_of_stars
        tree_string += space + stars + "\n"
    return tree_string[:-1]


if __name__ == "__main__":
    """
    Main function that runs the program. 
    Keep asking for input until the user provides an integer
    If the user ran the test command, then run the tests instead of the main program
    """
    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        print("-----Running Tree tests-----")
        test_suite = unittest.TestLoader().loadTestsFromTestCase(TestTreeMethod)
        unittest.TextTestRunner(verbosity=2).run(test_suite)
    else:
        while True:
            try:
                tree_height = int(input('Please, enter the Xmas tree height: '))
                tree = draw_tree(tree_height)
                print(tree)
                break
            except ValueError:
                print("Invalid input. Please, enter the Xmas tree height in integer format")
